package element;

/**Diese Klasse erzeugt ein Punktobjekt was zwei Koordinaten
 * entgegennimmt. Einmal x und einmal y als Integerwert */
public class Punkt {
	
	private final int posx, posy;
	
	/** Erzeugt ein Punkt
	 * 
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 */
	public Punkt(int x, int y){
		posx = x;
		posy = y;
	}
	
	/**
	 * gibt den x-Wert zurueck
	 * @return x x
	 */
	public int getX(){
		return posx;
	}
	
	/**
	 * gibt den y-Wert zurueck
	 * @return y y
	 */
	public int getY(){
		return posy;
	}

}
