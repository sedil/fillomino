package gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Regel extends Stage {

	private VBox layout;
	private TextArea text;
	private ScrollPane scroll;
	private Button ok;
	private final String eins = "Felder mit gleichen Zahlen muessen vertikal und/oder horizontal zusammenhaengende Gebiete (sog. Minobloecke) bilden, die aus genau so vielen Feldern bestehen, wie die Zahl angibt. Der Zahlenbereich liegt zwischen 2 und 10, wobei nicht jede Zahl unbedingt vorkommen muss. Die 1ser-Felder werden vorgegeben.\n";
	private final String zwei = "Zwei verschiedene, horizontal oder vertikal zusammenstossende Minobloecke duerfen nicht die gleiche Groesse, also gleiche Zahl, haben.\n";
	private final String drei = "Zwei Felder mit der gleichen vorgegebenen Zahl koennen zum gleichen Minoblock gehoeren, muessen es aber nicht!\n";
	private final String vier = "Felder die leer sind muessen mit Minos zu Minobloecken ergaenzt werden. Dabei kann der Fortschritt mit 'Raetsel -> Pruefe' zu jederzeit ueberprueft werden. Gruene Felder zeigen an, dass diese Felder richtig gewaehlt wurden. Rote Felder hingegen sind entweder leere Felder bzw. wurden diese Felder mit falschen Zahlen befuellt. Es gibt nur eine Loesung!";
	
	Regel(){
		initModality(Modality.APPLICATION_MODAL);
		komponentenInit();
		Scene inhalt = new Scene(layout);
		setScene(inhalt);
		sizeToScene();
		setTitle("Spielregeln");
		setResizable(false);
		show();
	}

	private void komponentenInit(){
		layout = new VBox();
		text = new TextArea(eins+""+zwei+""+drei+""+vier);
		text.setWrapText(true);
		scroll = new ScrollPane(text);
		ok = new Button("Alles klar!");
		ok.setOnAction(new KnopfHandler());
		layout.getChildren().addAll(scroll,ok);

	}

	private class KnopfHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent action) {
			if ( action.getSource() == ok){
				close();
			}
		}
	}

}
