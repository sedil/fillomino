package gui;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Diese Klasse beinhaltet die Main-Methode
 */
public class Start extends Application {
	public static void main (String[] args){
		launch(args);
	}

	/** ueberschriebene Methode aus Application */
	@Override
	public void start(Stage arg0) throws Exception {
		new Hauptmenu();		
	}

}
