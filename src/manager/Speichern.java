package manager;

import java.io.File;
import java.io.FileWriter;

import element.Mino;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class Speichern {
	
	private String speichertext;
	private FileChooser fc;

	public Speichern(int[] feld, Mino[][] a, Mino[][] l){
		speichertext = new String();
		
		fc = new FileChooser();
		fc.setTitle("Fortschritt speichern...");
		fc.getExtensionFilters().addAll(new ExtensionFilter("FRD-Datei", "*.frd"));
		
		sammelDatenZumSpeichern(feld,a,l);
		speichern();
	}
	
	private void sammelDatenZumSpeichern(int[] feldgroesse, Mino[][] aktuell, Mino[][] loesung){
		String feld = "#FELD\r\n"+feldgroesse[0]+" X "+feldgroesse[1]+"";
		speichertext += feld+"\r\n";

		String akt = "#AKT\r\n";
		for(int x = 0; x < feldgroesse[0]; x++){
			for(int y = 0; y < feldgroesse[1]; y++){
				akt += "< "+x+" , "+y+" , "+aktuell[x][y].getWert()+" | "+aktuell[x][y].getStarter()+" > \r\n";
			}
		}
		speichertext += akt;
		
		String lsg = "#LSG\r\n";
		for(int x = 0; x < feldgroesse[0]; x++){
			for(int y = 0; y < feldgroesse[1]; y++){
				lsg += "< "+x+" , "+y+" , "+loesung[x][y].getWert()+" | "+loesung[x][y].getStarter()+" > \r\n";
			}
		}
		speichertext += lsg;
	}
	
	private void speichern(){
		File speicherdatei = fc.showSaveDialog(null);
		
		if ( speicherdatei != null){
			try {
				FileWriter schreiber = new FileWriter(speicherdatei.getPath());
				schreiber.write(speichertext);
				schreiber.flush();
				schreiber.close();
				
				Alert f = new Alert(AlertType.INFORMATION);
				f.setHeaderText("Speichern erfolgreich");
				f.setContentText("Fortschritt wurde erfolgreich gespeichert");
				f.showAndWait();			
			} catch (Exception e){
				Alert f = new Alert(AlertType.ERROR);
				f.setHeaderText("Speichern fehlgeschlagen");
				f.setContentText("Fehler beim speichern");
				f.showAndWait();
			}
		}
	}

}
