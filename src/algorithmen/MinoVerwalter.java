package algorithmen;

import element.Mino;
import element.Punkt;
import gui.Fenster;
import gui.Zahleneingabe;

/** Diese Klasse verwaltet die Datenstrukturen, wo informationen uber den
 * aktuellen Spielfortschritt gesammelt und veraendert werden
 */
public class MinoVerwalter {
	
	private Fenster fenster;
	private int breite, hoehe;
	private Mino[][] aktuell, loesung;
	private final int RICHTIG = 11;
	private final int FALSCH = 12;

	/**
	 * Der MinoVerwalter-Konstruktor bekommt eine Fensterreferrenz uebergeben
	 * @param f fensterreferrenz
	 */
	public MinoVerwalter(Fenster f){
		fenster = f;
	}
	
	/** uebergibt die noetigen Informationen fuer die Datenstruktur
	 * 
	 * @param b raetselbreite
	 * @param h raetselhoehe
	 * @param akt aktuelles (ungeloestes) Raetsel
	 * @param lsg raetselloesung
	 */
	public void setMinoDaten(int b, int h, Mino[][] akt, Mino[][] lsg){
		breite = b;
		hoehe = h;
		aktuell = akt;
		loesung = lsg;
	}
	
	/** loescht jeglichen Fortschritt und setzt das Fillominoraetsel
	 * zurueck, als ob es gerade generiert wurde */
	public void zuruecksetzen(){
		for(int x = 0; x < breite; x++){
			for(int y = 0; y < hoehe; y++){
				if ( !aktuell[x][y].getStarter() ){
					aktuell[x][y].setWert(0);
				}
			}
		}
		fenster.getBild().zeichnen(fenster.getFarbe());
	}
	
	/** nimmt die Benutzereingabe (Mausklick) entgegen und stellt ein Fenster
	 * zur verfuegung, damit der Spieler ein Minoblock erzeugen kann
	 * @param x zelle x
	 * @param y zelle y
	 * @param p die Maustaste die geklickt wurde
	 */
	public void verarbeite(int x, int y, boolean p){
		if ( !fenster.getZwischenstand() & p ){
			Zahleneingabe z = new Zahleneingabe();
			int r = z.getEingabewert();
			if ( r >= 2 & r <= 10 & aktuell[x][y].getWert() == 0){
				minoHinzufuegen(x,y,r);
			} else if ( r >= 2 & r <= 10 & aktuell[x][y].getWert() > 0){
				minoVeraendern(x,y,r);
			}
		} else if ( !(fenster.getZwischenstand() & p) ) {
			entferneMino(x,y);
		}
		fenster.getBild().zeichnen(fenster.getFarbe());
	}
	
	/**
	 * fuegt ein Mino auf das entsprechende Feld ein, wenn es sich um
	 * eine freie Position handelt
	 * @param x x-Position
	 * @param y y-Position
	 * @param wert minowert
	 */
	private void minoHinzufuegen(int x, int y, int wert){
		if ( aktuell[x][y].getWert() < 1){
			aktuell[x][y].setWert(wert);
		}
	}
	
	/**
	 * veraendert ein Mino auf das entsprechende Feld, wenn es sich um
	 * ein bereits besetztes Feld handelt
	 * @param x x-Position
	 * @param y y-Position
	 * @param wert neuer minowert
	 */
	private void minoVeraendern(int x, int y, int wert){
		if ( aktuell[x][y].getStarter() == false & aktuell[x][y].getWert() != wert){
			aktuell[x][y].setWert(wert);
		}
	}

	/**
	 * entfernt ein Mino aus der uebergebenen Position aus den Feld samt aus
	 * der Minogruppe
	 * @param x x-Koordinate des zu entfernenden Minos
	 * @param y y-Koordinate des zu entfernenden Minos
	 */
	private void entferneMino(int x, int y){
		if (aktuell[x][y].getWert() > 0 & aktuell[x][y].getStarter() == false ){
			aktuell[x][y].setWert(0);
		}	
	}
	
	/**
	 * zeigt an, welche Felder richtig befuellt sind (Gruen) und welche Felder
	 * noch leer bzw. falsch befuellt sind (Rot)
	 */
	public void zwischenergebnis(){
		if ( fenster.zwischenstandWechsel() ){
			Mino[][] temp = aktuell;
			Mino[][] zwischen = new Mino[breite][hoehe];
			for(int x = 0; x < breite; x++){
				for(int y = 0; y < hoehe; y++){
					if ( loesung[x][y].getWert() == aktuell[x][y].getWert() ){
						zwischen[x][y] = new Mino(new Punkt(x,y),RICHTIG);
					} else {
						zwischen[x][y] = new Mino(new Punkt(x,y),FALSCH);
					}
				}
			}		
			aktuell = zwischen;
			fenster.getBild().zeichnen(fenster.getFarbe());
			aktuell = temp;
		} else {
			fenster.getBild().zeichnen(fenster.getFarbe());
		}
	}
	
	/**
	 * gibt die Raetselgroesse zurueck
	 * @return breite und hoehe
	 */
	public int[] getRaetselgroesse(){
		return new int[]{breite,hoehe};
	}
	
	/**
	 * gibt das 2D-Array, das aktuelle Raetsel, zurueck
	 * @return raetsel
	 */
	public Mino[][] getMinoFeld(){
		return aktuell;
	}
	
	/**
	 * gibt das 2D-Array, das geloeste Raetsel, zurueck
	 * @return raetsel
	 */
	public Mino[][] getLoesung(){
		return loesung;
	}

}
